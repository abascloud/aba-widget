# aba-widget
## Description
Wrapper for an dashboard widget

## Slots
1. content - Here goes the Content of your widget
1. optionsMenu - You can use this slot to add new options to the three dot menu of your widget
1. settingsForm - This slot is for the Settings of your widget

## Properties
* widgetId { String } UUID of this widget
* editMode { Boolean } 
* fallbackLanguage { String }

## Getter & Setter
* GET widgetMenu - Returns Element reference for the three dot menu
  
## Observer
* _editModeChanged(new, old) - Observes the editMode Property
  
## Event Listeners
* showSettings(event) - Open the settings for this widget
* showRemoveWidgetDialog(event) - Open the remove widget dialoge for this widget
* fireRemoveWidget(event) - Trigger removal of this widget
* clone(event) - Clone this widget on to the next free spot
* fireSettingsClosed(event) - Close the settings dialoge
* _settingsOpened(event) - Called when settings are finally open
* _blockDrag(event) - Block all drag events